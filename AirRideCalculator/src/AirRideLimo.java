
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class AirRideLimo extends Application {

public static void main(String[] args) {
launch(args);
}

@Override
public void start(Stage primaryStage) throws Exception {
	
// Create & configure user interface controls

// Labels
      Label TitleLabel = new Label("Airport Ride Calculator");
      Label LocationLabel = new Label("From:");
      TextField LocationTextBox = new TextField();

// Checkboxes
      CheckBox checkbox1 = new CheckBox();
      checkbox1.setText("Extra Luggage?");

      CheckBox checkbox2 = new CheckBox();
      checkbox2.setText("Pets");

      CheckBox checkbox3 = new CheckBox();
      checkbox3.setText("Use 407 ETR?");

      CheckBox checkbox4 = new CheckBox();
      checkbox4.setText("Add Tip?");

// Calculate Button
    Button btn = new Button();
    btn.setText("CALCULATE");

// Results Label
     Label resultsLabel = new Label("");

// Add event handler for button
btn.setOnAction(new EventHandler<ActionEvent>() {
@Override
public void handle(ActionEvent e) {
// Logic for what should happen when you push button
// Get what the person typed in the text boxes
	  double Basefare = 0.0;
	  double AdditionalCharges = 0.0;
	  double TipPrice = 0.0;
	  double Etrfare = 0.0;
      String name = LocationTextBox.getText();

// test that you got all the data correctly
      if(name.equalsIgnoreCase("Cestar College")) {
         Basefare = 51.00;
         Etrfare = 10.00;
   } 
      else if(name.equalsIgnoreCase("Brampton")){
    	  Basefare = 38.00;
          Etrfare = 5.00;
   } 
      if(checkbox1.isSelected()) {
        AdditionalCharges=+10;
   }
      if(checkbox2.isSelected()) {
        AdditionalCharges=+6;
   }
      if(checkbox3.isSelected()) {
        AdditionalCharges=+(Etrfare*0.25);
   }
      double totalprice = Basefare+AdditionalCharges;
      double tax = totalprice*0.13;
      double finalprice = totalprice*tax;
      
      if (checkbox4.isSelected()) {
    	  TipPrice = finalprice*0.15;
    	  finalprice=+TipPrice;
    }
      
// Output the result to the screen
       resultsLabel.setText("The total fare is: $" +finalprice);
    }
  });

//  Make a layout manager
    VBox root = new VBox();
    root.setSpacing(10);

// Add controls to the layout manager
    root.getChildren().add(TitleLabel);
    root.getChildren().add(LocationLabel);
    root.getChildren().add(LocationTextBox);
    root.getChildren().add(checkbox1);
    root.getChildren().add(checkbox2);
    root.getChildren().add(checkbox3);
    root.getChildren().add(checkbox4);
    root.getChildren().add(btn);
    root.getChildren().add(resultsLabel);

// Add layout manager to scene
// Add scene to a stage
// Set the width & height of application
     primaryStage.setScene(new Scene(root, 250, 300));
// Setting the title bar of the application
     primaryStage.setTitle("Air Ride Calulator");     
// Show the application
     primaryStage.show();
     }
  }

